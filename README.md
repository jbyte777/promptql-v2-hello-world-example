This is an example of multiagent network powered by the [PromptQL library](https://gitlab.com/jbyte777/prompt-ql/-/tree/release-2.x).

Here's an example sessions:

1. 1st image:
<img src="./docs/imgs/promptql-v2-showcase.png" />
<img src="./docs/imgs/stable-diffusion-img.png" />

2. 2nd image:
<img src="./docs/imgs/promptql-v2-showcase-2.png" />
<img src="./docs/imgs/stable-diffusion-img-2.png" />

3. 3rd image:
<img src="./docs/imgs/promptql-v2-showcase-3.png" />
<img src="./docs/imgs/stable-diffusion-img-3.png" />
