package useragent

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	fileutils "gitlab.com/jbyte777/promptql-v2-hello-world-example/utils/files"
	"path/filepath"
)

var consoleReader = bufio.NewReader(os.Stdin)

func getFirstPromptFromConsole(_args []interface{}) interface{} {
	fmt.Println("Hi! This is an example of multi-agent framework driven by PromptQL. Type your concept for Stable Diffusion image generation:")
	fmt.Print("> ")
	line, _ := consoleReader.ReadString('\n')
	if len(line) > 0 {
		line = line[:len(line) - 1]
	}
	return fmt.Sprintf(
		`
			{~session_begin /}
			{~open_query to="querysd" model="stable-diff"}
			  {~open_query to="querygpt" model="gpt-3.5-turbo-16k"}
				  {~system}
					  You're a text-to-image prompt engineer.
					  You know Stable Diffusion model.
				  {/system}
				  Generate a text-to-image prompt for Stable Diffusion that has this concept:
				  %v

					Include the resulting prompt in quotes:
					""
			  {/open_query}
			  {~call fn=@wrapgptres}{~listen_query from="querygpt" /}{~data}%v{/data}{/call}
		`,
		line,
		line,
	)
}

func propagateSDPrompt(args []interface{}) interface{} {
	if len(args) < 2 {
		return ``
	}

	prompt, _ := args[0].(string)
	concept, _ := args[1].(string)
	fmt.Printf("Here is your prompt:\n%v\n\n", prompt)
	fmt.Println("Is it good? (yes/no)")
	
	fmt.Print("> ")
	line, _ := consoleReader.ReadString('\n')
	if len(line) > 0 {
		line = line[:len(line) - 1]
	}

	if line == "yes" {
		return fmt.Sprintf(
			`
					{~user}%v{/user}
				{/open_query}
				{~call fn=@wrapsdres}{~listen_query from="querysd" /}{/call}
			`,
			prompt,
		)
	}

	return fmt.Sprintf(
		`
			{~open_query to="querygpt" model="gpt-3.5-turbo-16k"}
			  {~system}
					You're a text-to-image prompt engineer.
					You know Stable Diffusion model.
				{/system}
				{~system}
					"%v"
				  The prompt above is not good enough.
				{/system}
				Generate another(!) text-to-image prompt for Stable Diffusion that has this concept:
				%v

				Include the resulting prompt in quotes:
				""
			{/open_query}
			{~call fn=@wrapgptres}{~listen_query from="querygpt" /}{~data}%v{/data}{/call}
		`,
		prompt,
		concept,
		concept,
	)
}

func saveSDLink(args []interface{}) interface{} {
	if len(args) < 1 {
		return ``
	}

  link, _ := args[0].(string)
	resChan := make(chan bool)
	errChan := make(chan error)

	go func() {
		client := &http.Client{}
		request, _ := http.NewRequest(
			"GET",
			link,
			nil,
		)
		response, err := client.Do(request)
		if err != nil {
			errChan <- fmt.Errorf(
				"ERROR: %v",
				err.Error(),
			)
			return
		}
		if response.StatusCode >= 400 {
			errChan <- fmt.Errorf(
				"ERROR: %v",
				err.Error(),
			)
			return
		}

		rawResBody, err := ioutil.ReadAll(response.Body)
		if err != nil {
			errChan <- fmt.Errorf(
				"ERROR: %v",
				err.Error(),
			)
			return
		}

		fileExt := fileutils.GetFileExtension(link)
		pathToSave := filepath.Join(
			"savedimgs",
			fmt.Sprintf(
				"stable-diffusion-img.%v",
				fileExt,
			),
		)
		err = ioutil.WriteFile(
			pathToSave,
			rawResBody,
			0666,
		)
		if err != nil {
			errChan <- fmt.Errorf(
				"ERROR: %v",
				err.Error(),
			)
			return
		}

		resChan <- true
	}()

	timer := time.NewTimer(time.Second * 15)
	select {
	case <- resChan:
		fmt.Println("Generated image is stored in the \"savedimgs\" folder")
		return `{~session_end /}`
	case err := <- errChan:
		fmt.Printf("ERROR: %v\n", err.Error())
		return ``
	case <- timer.C:
		fmt.Printf("ERROR: timeout for saving SD picture exceed\n")
		return ``
	}
}

func bye(args []interface{}) interface{} {
	fmt.Println("Bye!")
	return ``
}
