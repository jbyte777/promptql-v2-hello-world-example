package useragent

import (
	"time"
	"sync"

	pqlcore "gitlab.com/jbyte777/prompt-ql/v5/core"
	promptql "gitlab.com/jbyte777/prompt-ql/v5/interpreter"
	agent "gitlab.com/jbyte777/promptql-v2-hello-world-example/agent"
)

type TUserAgent struct {
	interpreter *promptql.PromptQL
	ownerChan chan string
	isOwnerClosed bool
	peer agent.TAgent
}

func NewUserAgent() *TUserAgent {
	defaultGlobals := pqlcore.TGlobalVariablesTable{
		"firstprompt": getFirstPromptFromConsole,
		"sdprompt": propagateSDPrompt,
		"sdlink": saveSDLink,
		"bye": bye,
	}
	interpreter := promptql.New(
		promptql.PromptQLOptions{
			DefaultExternalGlobals: defaultGlobals,
		},
	)

	return &TUserAgent{
		interpreter: interpreter,
		ownerChan: make(chan string),
		isOwnerClosed: true,
		peer: nil,
	}
}

func (self *TUserAgent) GetCmdChan() (chan string) {
	return self.ownerChan
}

func (self *TUserAgent) ConnectPeer(peer agent.TAgent) {
	self.peer = peer
	self.isOwnerClosed = false
}

func (self *TUserAgent) IsAgentClosed() bool {
	return self.isOwnerClosed
}

func (self *TUserAgent) InitAgent(rootWg *sync.WaitGroup) {
	// Initiative agent goroutine
	go func(self *TUserAgent) {
		interpreter := self.interpreter.Instance
		peerChan := self.peer.GetCmdChan()

		// For initiative agent
		if self.peer.IsAgentClosed() {
			return
		}

		// Init block, for initiative agent
		if interpreter.IsSessionClosed() {
			pqlResult := interpreter.Execute(
				`{~session_begin /}{~call fn=@firstprompt /}`,
			)
			resData := pqlResult.ResultLatestData("data")
			resUser := pqlResult.ResultLatestData("user")
			nextCmdForPeer := ""
			if resData != nil {
				nextCmdForPeer, _ = resData.(string)
			} else if resUser != nil {
				nextCmdForPeer, _ = resUser.(string)
			}

			peerChan <- nextCmdForPeer
		}

		for cmd := range self.ownerChan {
			time.Sleep(time.Millisecond * 500)

			// For initiative agent
			if self.peer.IsAgentClosed() {
				interpreter.Execute(
					`{~session_end /}{~call fn=@bye /}`,
				)
				self.peer = nil
				self.isOwnerClosed = true
				break
			}

			pqlResult := interpreter.Execute(cmd)
			resData := pqlResult.ResultLatestData("data")
			resUser := pqlResult.ResultLatestData("user")

			nextCmdForPeer := ""
			isResultStr := false
			if resData != nil {
				nextCmdForPeer, isResultStr = resData.(string)
			} else if resUser != nil {
				nextCmdForPeer, isResultStr = resUser.(string)
			}

			if !isResultStr {
				peerChan <- ``
			} else {
				peerChan <- nextCmdForPeer
			}

			if interpreter.IsSessionClosed() {
				self.peer = nil
				self.isOwnerClosed = true
				break
			}
		}

		rootWg.Done()
	}(self)
}
