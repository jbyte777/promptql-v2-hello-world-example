package files

func GetFileExtension(path string) string {
	ptr := len(path) - 1
	for ptr >= 0 && path[ptr] != '.' {
		ptr--
	}

	if ptr < 0 || ptr >= len(path) - 1 {
		return ""
	}

	return path[ptr+1:]
}
