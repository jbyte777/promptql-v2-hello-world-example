package agent

import (
	"sync"
)

type TAgent interface {
	GetCmdChan() (chan string)
	ConnectPeer(peer TAgent)
	IsAgentClosed() bool
	InitAgent(rootWg *sync.WaitGroup)
}
