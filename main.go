package main

import (
	"encoding/json"
	"io/ioutil"
	"sync"
	mlagent "gitlab.com/jbyte777/promptql-v2-hello-world-example/ml-agent"
	useragent "gitlab.com/jbyte777/promptql-v2-hello-world-example/user-agent"
	types "gitlab.com/jbyte777/promptql-v2-hello-world-example/types"
)

func main() {
	file, err := ioutil.ReadFile("config.json")
	if err != nil {
		panic(err)
	}

	var config types.TConfig
	err = json.Unmarshal(file, &config)
	if err != nil {
		panic(err)
	}

	userAgent := useragent.NewUserAgent()
	mlAgent := mlagent.NewMLAgent(
		config.OpenAiBaseUrl,
		config.OpenAiKey,
		config.SdBaseUrl,
		config.SdKey,
	)

	userAgent.ConnectPeer(mlAgent)
	mlAgent.ConnectPeer(userAgent)

	waitGroup := &sync.WaitGroup{}
	waitGroup.Add(2)

	mlAgent.InitAgent(waitGroup)
	userAgent.InitAgent(waitGroup)

	waitGroup.Wait()
}
