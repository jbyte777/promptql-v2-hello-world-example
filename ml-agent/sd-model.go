package mlagent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"io/ioutil"

	pqlcore "gitlab.com/jbyte777/prompt-ql/v5/core"
	customapis "gitlab.com/jbyte777/prompt-ql/v5/custom-apis"
)

func makeSDModelApi(
	sdUrl string,
	sdToken string,
) customapis.TDoQueryFunc {
	return func(
		model string,
		temperature float64,
		inputs pqlcore.TFunctionInputChannelTable,
		execInfo pqlcore.TExecutionInfo,
	) (string, error) {
		userChan := inputs["user"]
		rawLatestPrompt := userChan.LatestCleanData()
		latestPrompt, _ := rawLatestPrompt.(string)

		client := &http.Client{}
		reqUrl := fmt.Sprintf(
			"%v/api/v3/text2img",
			sdUrl,
		)
		reqJson, _ := json.Marshal(
			tSDPromptRequest{
				Key:               sdToken,
				Prompt:            latestPrompt,
				Width:             512,
				Height:            512,
				Samples:           4,
				NumInferenceSteps: 20,
				GuidanceScale:     7.5,
			},
		)
		request, _ := http.NewRequest(
			"POST",
			reqUrl,
			bytes.NewBuffer(reqJson),
		)
		request.Header.Add("Content-Type", "application/json")
		response, err := client.Do(request)
		if err != nil {
			return "", fmt.Errorf(
				"ERROR (line=%v, charpos=%v): %v",
				execInfo.Line,
				execInfo.CharPos,
				err.Error(),
			)
		}
		if response.StatusCode >= 400 {
			return "", fmt.Errorf(
				"ERROR (line=%v, charpos=%v): %v",
				execInfo.Line,
				execInfo.CharPos,
				err.Error(),
			)
		}

		rawResBody, _ := ioutil.ReadAll(response.Body)
		var resBody tSDPromptResponse
		_ = json.Unmarshal(rawResBody, &resBody)

		return resBody.Output[0], nil
	}
}
