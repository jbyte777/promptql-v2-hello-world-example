package mlagent

import (
	"time"
	"sync"
	// "fmt"

	pqlcore "gitlab.com/jbyte777/prompt-ql/v5/core"
	promptql "gitlab.com/jbyte777/prompt-ql/v5/interpreter"
	agent "gitlab.com/jbyte777/promptql-v2-hello-world-example/agent"
)

type TMLAgent struct {
	interpreter *promptql.PromptQL
	ownerChan chan string
	isOwnerClosed bool
	peer agent.TAgent
}

func NewMLAgent(
	openAiUrl string,
	openAiToken string,
	sdUrl string,
	sdToken string,
) *TMLAgent {
	defaultGlobals := pqlcore.TGlobalVariablesTable{
		"wrapgptres": getGptPromptResWrap,
		"wrapsdres": getSDResWrap,
	}
	interpreter := promptql.New(
		promptql.PromptQLOptions{
			OpenAiBaseUrl: openAiUrl,
			OpenAiKey: openAiToken,
			OpenAiListenQueryTimeoutSec: 40,
			CustomApisListenQueryTimeoutSec: 150,
			DefaultExternalGlobals: defaultGlobals,
		},
	)
	interpreter.CustomApis.RegisterModelApi(
		"stable-diff",
		makeSDModelApi(sdUrl, sdToken),
		"",
	)

	return &TMLAgent{
		interpreter: interpreter,
		ownerChan: make(chan string),
		isOwnerClosed: true,
		peer: nil,
	}
}

func (self *TMLAgent) GetCmdChan() (chan string) {
	return self.ownerChan
}

func (self *TMLAgent) ConnectPeer(peer agent.TAgent) {
	self.peer = peer
	self.isOwnerClosed = false
}

func (self *TMLAgent) IsAgentClosed() bool {
	return self.isOwnerClosed
}

func (self *TMLAgent) InitAgent(rootWg *sync.WaitGroup) {
	// Service agent goroutine
	go func(self *TMLAgent) {
		interpreter := self.interpreter.Instance
		peerChan := self.peer.GetCmdChan()

		for cmd := range self.ownerChan {
			time.Sleep(time.Millisecond * 500)

			pqlResult := interpreter.Execute(cmd)
			resData := pqlResult.ResultLatestData("data")
			resUser := pqlResult.ResultLatestData("user")

			nextCmdForPeer := ""
			isResultStr := false
			if resData != nil {
				nextCmdForPeer, isResultStr = resData.(string)
			} else if resUser != nil {
				nextCmdForPeer, isResultStr = resUser.(string)
			}

			if !isResultStr {
				peerChan <- ``
			} else {
				peerChan <- nextCmdForPeer
			}

			if interpreter.IsSessionClosed() {
				self.peer = nil
				self.isOwnerClosed = true
				break
			}
		}

		rootWg.Done()
	}(self)
}
