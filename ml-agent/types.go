package mlagent

type tSDPromptRequest struct {
	Key string `json:"key"`
	Prompt string `json:"prompt"`
	Width int `json:"width"`
	Height int `json:"height"`
	Samples int `json:"Samples"`
	NumInferenceSteps int `json:"num_inference_steps"`
	GuidanceScale float64 `json:"guidanceScale"`
}

type tSDPromptResponse struct {
	Output []string `json:"output"`
}


