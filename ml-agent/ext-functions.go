package mlagent

import "fmt"

func getSDResWrap(args []interface{}) interface{} {
	link, _ := args[0].(string)
	
	return fmt.Sprintf(
		`{~call fn=@sdlink}%v{/call}`,
		link,
	)
}

func getGptPromptResWrap(args []interface{}) interface{} {
	prompt, _ := args[0].(string)
	concept, _ := args[1].(string)

	return fmt.Sprintf(
		`
			{~call fn=@sdprompt}{~data}%v{/data}{~data}%v{/data}{/call}
		`,
		prompt,
		concept,
	)
}
