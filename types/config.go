package types

type TConfig struct {
	OpenAiBaseUrl string `json:"openAiBaseUrl"`
  OpenAiKey string `json:"openAiKey"`
  SdBaseUrl string `json:"sdBaseUrl"`
  SdKey string `json:"sdKey"`
}
